package com.kz.najanaja;


import java.sql.SQLOutput;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            while (true) {
                System.out.println("Enter 1 for homeWork1 part 1");
                System.out.println("Enter 2 for homeWork1 part 2");
                System.out.println("Enter 0 to Quit");

                String menu = in.nextLine();

                switch (menu) {
                    case "1" -> {
                        System.out.println("Enter for task 1");
                        System.out.println("Enter for task 2");
                        System.out.println("Enter for task 3");
                        System.out.println("Enter for task 4");
                        System.out.println("Enter for task 5");
                        String submenu1 = in.nextLine();
                        switch (submenu1) {
                            case "1":
                                System.out.println("Enter user name");
                                String name = in.nextLine();
                                System.out.println("Hello " + name);

                                break;

                            case "2":
                                System.out.println("Args in revers order");
                                for (int i = args.length - 1; i >= 0; i--) {
                                    System.out.println(args[i]);
                                }

                                break;

                            case "3":
                                Random random = new Random();
                                System.out.println("Enter count of numbers");
                                int count = in.nextInt();
                                for (int i = 0; i < count; i++) {
                                    if (i % 2 != 0) {
                                        System.out.print("Number is: " + random.nextInt(250 - 1) + 1);
                                    } else
                                        System.out.println("Number is: " + random.nextInt(250 - 1) + 1);
                                }
                                System.out.println();
                                break;

                            case "4":
                                double[] argsDouble = new double[args.length];
                                double result = 0;

                                for (int i = 0; i < args.length; i++) {
                                    argsDouble[i] = Double.parseDouble(args[i]);
                                    System.out.println(argsDouble[i]);
                                }

                                for (int i = 0; i < argsDouble.length; i++) {
                                    result += argsDouble[i];
                                }
                                System.out.println("Sum of args " + result);
                                break;

                            case "5":
                                System.out.println("Enter number of month");
                                int month = in.nextInt();
                                switch (month) {
                                    case 1 -> System.out.println("January");
                                    case 2 -> System.out.println("February");
                                    case 3 -> System.out.println("March");
                                    case 4 -> System.out.println("April");
                                    case 5 -> System.out.println("May");
                                    case 6 -> System.out.println("June");
                                    case 7 -> System.out.println("July");
                                    case 8 -> System.out.println("August");
                                    case 9 -> System.out.println("September");
                                    case 10 -> System.out.println("October");
                                    case 11 -> System.out.println("November");
                                    case 12 -> System.out.println("December");
                                    default -> System.out.println("Wrong month number!");
                                }
                                break;

                            default:
                                break;
                        }
                    }
                    case "2" -> {
                        System.out.println("Enter integer number");
                        long num = in.nextLong();
                        if (num <= 128 && num >= -128) {
                            System.out.println(num + " can be fitted in byte, short, int, long");
                        } else if (num <= 32768 && num >= -32768) {
                            System.out.println(num + " can be fitted in short, int, long");

                        } else if (num <= 2147483647 && num >= -2147483647) {
                            System.out.println(num + " can be fitted in int, long");

                        } else {
                            System.out.println(num + " can be fitted in int, long");
                        }

                    }
                    case "0" -> System.exit(0);
                    default -> System.out.println("Wrong menu number!");
                }
            }
        } catch (Exception ex) {
            System.out.println("Ex " + ex.getMessage());
        }
    }
}
